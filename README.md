# Test Symfony task for the job

Small symfony application that can upload .xml, .csv and .json files of fixed structure and parse them and persist the data inside a database. Application can do it from web part and from console and choose the format automatically. Samples of the files are in \var\files directory. 

## Run:
bin/console test:load FileName
FileName have be in the path defined in services.yaml 
or from browser.

## P.S.
I used Serializer for parsing and Doctrine for persist. No Twig. Some tests.
