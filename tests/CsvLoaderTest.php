<?php

namespace App\Tests;

use App\Entity\Person;
use App\Loaders\CsvLoader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\FileLocator;

class CsvLoaderTest extends TestCase
{

    public function testLoadCsv(): void
    {
        $rightPerson = new Person();
        $rightPerson->setName('Luke Skywalker');
        $rightPerson->setHeight(172);
        $rightPerson->setMass(77);
        $rightPerson->setHaircolor('blond');
        $rightPerson->setSkincolor('fair');
        $rightPerson->setEyecolor('blue');
        $rightPerson->setBirthyear('19BBY');
        $rightPerson->setGender('male');

        $locator = new FileLocator('');
        $testLoader = new CsvLoader($locator);
        $testPerson = $testLoader->load(__DIR__ . '/../Tests/TestData/fileTest.csv')[0];

        $this->assertTrue($testPerson == $rightPerson);
    }

    public function testLoadWrong(): void
    {
        $locator = new FileLocator('');
        $testLoader = new CsvLoader($locator);
        $testPerson = $testLoader->load(__DIR__ . '/../Tests/TestData/fileTestWrongType.txt');

        $this->assertTrue(sizeof($testPerson) == 0);
    }

}
