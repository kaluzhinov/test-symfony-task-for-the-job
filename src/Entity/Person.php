<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Person
{

    #[ORM\Id, ORM\GeneratedValue, ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'text')]
    protected string $name;

    #[ORM\Column(type: 'integer')]
    protected int $height;

    #[ORM\Column(type: 'integer')]
    protected int $mass;

    #[ORM\Column(type: 'text')]
    protected string $hair_color;

    #[ORM\Column(type: 'text')]
    protected string $skin_color;

    #[ORM\Column(type: 'text')]
    protected string $eye_color;

    #[ORM\Column(type: 'text')]
    protected string $birth_year;

    #[ORM\Column(type: 'text')]
    protected string $gender;



    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set the value of height
     *
     * @return  self
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get the value of mass
     */
    public function getMass()
    {
        return $this->mass;
    }

    /**
     * Set the value of mass
     *
     * @return  self
     */
    public function setMass($mass)
    {
        $this->mass = $mass;

        return $this;
    }

    /**
     * Get the value of hair_color
     */
    public function getHaircolor()
    {
        return $this->hair_color;
    }

    /**
     * Set the value of hair_color
     *
     * @return  self
     */
    public function setHaircolor($hair_color)
    {
        $this->hair_color = $hair_color;

        return $this;
    }

    /**
     * Get the value of skin_color
     */
    public function getSkincolor()
    {
        return $this->skin_color;
    }

    /**
     * Set the value of skin_color
     *
     * @return  self
     */
    public function setSkincolor($skin_color)
    {
        $this->skin_color = $skin_color;

        return $this;
    }

    /**
     * Get the value of eye_color
     */
    public function getEyecolor()
    {
        return $this->eye_color;
    }

    /**
     * Set the value of eye_color
     *
     * @return  self
     */
    public function setEyecolor($eye_color)
    {
        $this->eye_color = $eye_color;

        return $this;
    }

    /**
     * Get the value of birth_year
     */
    public function getBirthyear()
    {
        return $this->birth_year;
    }

    /**
     * Set the value of birth_year
     *
     * @return  self
     */
    public function setBirthyear($birth_year)
    {
        $this->birth_year = $birth_year;

        return $this;
    }

    /**
     * Get the value of gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set the value of gender
     *
     * @return  self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }
}
