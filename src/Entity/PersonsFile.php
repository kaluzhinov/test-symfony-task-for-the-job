<?php

namespace App\Entity;

use App\Entity\Person;

class PersonsFile
{

    protected int $count;

    protected array $results;
    

    /**
     * Get the value of results
     */ 
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Set the value of results
     *
     * @return  self
     */ 
    public function setResults($results)
    {
        $this->results = $results;

        return $this;
    }

    /**
     * Get the value of count
     */ 
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set the value of count
     *
     * @return  self
     */ 
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }
}
