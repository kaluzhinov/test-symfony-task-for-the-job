<?php

namespace App\Loaders;

use App\Entity\PersonsFile;
use App\Entity\Person;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class XmlLoader extends FileLoader
{
    public function load($resource, $type = null): mixed
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new XmlEncoder()]);
        $data = $serializer->deserialize(file_get_contents($resource), PersonsFile::class, 'xml');
        foreach ($data->getResults()['result'] as $value) {
            $person = $serializer->denormalize($value, Person::class);
            $persons[] = $person;
        }

        return $persons;
    }

    public function supports($resource, $type = null): bool
    {
        $file = fopen($resource, "r");
        $str = fgets($file);
        fclose($file);
        return strpos($str, '?xml version=');
    }
}
