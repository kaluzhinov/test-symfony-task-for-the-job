<?php

namespace App\Loaders;

use App\Entity\Person;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class CsvLoader extends FileLoader
{

    public function load($resource, $type = null): mixed
    {

        $content = file_get_contents($resource);
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        $data = $serializer->decode($content, 'csv');
        $persons = [];
        foreach ($data as $value) {
            $person = $serializer->denormalize($value, Person::class);
            $persons[] = $person;
        }

        return $persons;
    }

    public function supports($resource, $type = null): bool
    {
        $file = fopen($resource, "r");
        $str = fgets($file);
        fclose($file);
        return strpos($str, 'ame,');
    }
}
