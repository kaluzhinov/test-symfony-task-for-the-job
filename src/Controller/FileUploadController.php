<?php

namespace App\Controller;

use App\Service\InputFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class FileUploadController extends AbstractController
{
    private InputFileLoader $loader;

    public function __construct(
        InputFileLoader $loader

    ) {
        $this->loader = $loader;
    }
    /**
     * @Route("/file/upload", name="app_file_new")
     */
    public function new(Request $request)
    {
        $file = $request->files->get('file');
        $file->move($this->getParameter('filepath')[0], $file->getfilename());

        $this->loader->loadFile($file->getFilename());

        return new Response('Upload successful');
    }
}
