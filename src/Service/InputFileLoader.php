<?php

namespace App\Service;

use App\Loaders\CsvLoader;
use App\Loaders\XmlLoader;
use App\Loaders\JsonLoader;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\Config\Loader\DelegatingLoader;



class InputFileLoader
{
    protected $locator;

    public function __construct(FileLocator $locator)
    {
        $this->locator = $locator;
    }

    public function loadFile(string $fileName): ?array
    {
        $userFile = $this->locator->locate($fileName, null, false);
        if (is_array($userFile)) $userFile = (string) $userFile[0];

        $fileTypes = [new XmlLoader($this->locator), new JsonLoader($this->locator), new CsvLoader($this->locator)];
        $loaderResolver = new LoaderResolver($fileTypes);
        $delegatingLoader = new DelegatingLoader($loaderResolver);

        try {
            return  $delegatingLoader->load($userFile);
        } catch (\Exception $exception) {
            dd("LOAD FILE ERROR");
        }
    }
}
