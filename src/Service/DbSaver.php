<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class DbSaver
{

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function personsSave($persons)
    {
        try {

            foreach ($persons as $value) {
                $this->entityManager->persist($value);
            }
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            echo 'Some database error:' . $exception->getMessage();
        }
    }
}
