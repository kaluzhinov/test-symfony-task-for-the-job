<?php

namespace App\Command;

use Exeption;
use App\Service\DbSaver;
use App\Service\InputFileLoader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'test:load',
    description: 'Load and parse files for test',
)]
class TestFileLoadCommand extends Command
{

    private DbSaver $saver;
    private InputFileLoader $loader;

    public function __construct(
        DbSaver $saver,
        InputFileLoader $loader

    ) {
        parent::__construct();
        $this->saver = $saver;
        $this->loader = $loader;
    }

    protected function configure()
    {
        $this->setDefinition(
            new InputDefinition([
                new InputArgument('file', InputArgument::REQUIRED, 'A filename to parse'),
            ])
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $file = $input->getArgument('file');
        $this->saver->personsSave($this->loader->loadFile($file));
        return Command::SUCCESS;
    }
}
